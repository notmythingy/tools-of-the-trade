# Tools of The Trade

Collection of hacking tools from TCM Security's Python courses, Black Hat Python by Justin Seitz and Tim Arnold, and other random sources.
Some are modified with moderation, some heavily and some I may have written all by myself!
